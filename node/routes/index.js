var express = require("express");
var router = express.Router();

// lowdb
const low = require("lowdb");
const lodashId = require("lodash-id");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("db.json");
const db = low(adapter);

db._.mixin(lodashId);

// Set some defaults
db.defaults({ users: [], labs: [], orders: [] }).write();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.post("/login", function (req, res, next) {
  console.log("req.body", req.body);
  const { username, password } = req.body;

  const record = db.get("users").find({ username }).value();
  if (!record) {
    return res.send({
      error_code: -1,
      error_msg: "用户名不存在",
      data: null,
    });
  }
  if (record.password !== password) {
    return res.send({
      error_code: -1,
      error_msg: "密码错误",
      data: null,
    });
  }
  res.send({
    error_code: 0,
    error_msg: "",
    data: true,
  });
});

router.post("/register", function (req, res, next) {
  const { username, password } = req.body;
  const record = db.get("users").find({ username }).value();
  if (record) {
    return res.send({
      error_code: -1,
      error_msg: "用户名已存在",
      data: null,
    });
  }
  db.get("users").insert({ username, password }).write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: true,
  });
});

// 增加实验室
router.post("/labs", function (req, res, next) {
  const { labName, position, deviceNum } = req.body;
  const record = db.get("labs").find({ labName }).value();
  if (record) {
    return res.send({
      error_code: -1,
      error_msg: "实验室已存在",
      data: null,
    });
  }
  const editRecord = db
    .get("labs")
    .insert({ labName, position, deviceNum })
    .write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: editRecord,
  });
});

// 删除实验室
router.delete("/labs/:id", function (req, res, next) {
  const id = req.params.id;
  db.get("labs").remove({ id }).write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: true,
  });
});

// 编辑实验室
router.put("/labs/:id", function (req, res, next) {
  const id = req.params.id;
  const { labName, position, deviceNum } = req.body;
  const record = db.get("labs").find({ labName }).value();
  if (record && record.id !== id) {
    return res.send({
      error_code: -1,
      error_msg: "实验室名已存在",
      data: null,
    });
  }
  const editRecord = db
    .get("labs")
    .find({ id })
    .assign({ labName, position, deviceNum })
    .write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: editRecord,
  });
});

// 查询实验室
router.get("/labs", function (req, res, next) {
  const records = db.get("labs").value();
  res.send({
    error_code: 0,
    error_msg: "",
    data: records,
  });
});

// 增加预约记录
router.post("/orders", function (req, res, next) {
  const { labName, time, username } = req.body;
  const record = db
    .get("orders")
    .insert({ labName, time, username, status: "waiting" })
    .write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: record,
  });
});

// 删除预约记录
router.delete("/orders/:id", function (req, res, next) {
  const id = req.params.id;
  db.get("orders").remove({ id }).write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: true,
  });
});

// 查询预约记录
router.get("/orders", function (req, res, next) {
  const records = db.get("orders").value();
  res.send({
    error_code: 0,
    error_msg: "",
    data: records,
  });
});

// 通过预约记录
router.post("/orders/:id/approve", function (req, res, next) {
  const id = req.params.id;
  const { status } = req.body;
  db.get("orders").find({ id }).assign({ status }).write();
  res.send({
    error_code: 0,
    error_msg: "",
    data: true,
  });
});

module.exports = router;
