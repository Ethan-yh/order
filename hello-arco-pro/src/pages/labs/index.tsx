import React, { useEffect, useState, useCallback } from 'react';
import { Table, Button, Breadcrumb, Card } from '@arco-design/web-react';
import styles from './style/index.module.less';
import * as apis from '../../api';
import LabEditModal from './LabEditModal';
import OrderModal from './OrderModal';

function Labs() {
  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const username = localStorage.getItem('username');
  const [editMode, setEditMode] = useState<'create' | 'edit'>('create');
  const [curLab, setCurLab] = useState();
  const [editVisible, setEditVisible] = useState(false);
  const [orderVisible, setOrderVisible] = useState(false);

  const fetchData = useCallback(async () => {
    try {
      setLoading(true);
      const { data } = await apis.getLabs();
      console.log('data', data);
      setData(data);
    } finally {
      setLoading(false);
    }
  }, []);
  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const onCreate = useCallback(() => {
    setCurLab(undefined);
    setEditMode('create');
    setEditVisible(true);
  }, []);

  const onEdit = useCallback((lab: any) => {
    setCurLab(lab);
    setEditMode('edit');
    setEditVisible(true);
  }, []);

  const onOrder = useCallback((lab: any) => {
    setCurLab(lab);
    setOrderVisible(true);
  }, []);

  const onDelete = useCallback(
    async (lab: any) => {
      await apis.deleteLab(lab.id);
      fetchData();
    },
    [fetchData]
  );

  const columns = [
    {
      title: '实验室名称',
      dataIndex: 'labName',
    },
    {
      title: '位置',
      dataIndex: 'position',
    },
    {
      title: '可用设备数量',
      dataIndex: 'deviceNum',
    },
    {
      title: '操作',
      dataIndex: 'operations',
      render: (_, record: any) => (
        <div className={styles.operations}>
          <Button
            type="text"
            size="small"
            onClick={() => onEdit(record)}
            disabled={username !== 'admin'}
          >
            编辑
          </Button>
          <Button type="text" size="small" onClick={() => onOrder(record)}>
            预约
          </Button>
          <Button
            type="text"
            status="danger"
            size="small"
            onClick={() => onDelete(record)}
            disabled={username !== 'admin'}
          >
            删除
          </Button>
        </div>
      ),
    },
  ];

  return (
    <div className={styles.container}>
      <Breadcrumb style={{ marginBottom: 20 }}>
        <Breadcrumb.Item>实验室预约</Breadcrumb.Item>
        <Breadcrumb.Item>实验室列表</Breadcrumb.Item>
      </Breadcrumb>
      <Card bordered={false}>
        <div className={styles.toolbar}>
          <div>
            <Button type="primary" onClick={onCreate}>
              添加实验室
            </Button>
          </div>
        </div>
        <Table rowKey="id" loading={loading} columns={columns} data={data} />
      </Card>
      <LabEditModal
        mode={editMode}
        visible={editVisible}
        data={curLab}
        onCancel={() => setEditVisible(false)}
        onSuccess={() => {
          setEditVisible(false);
          fetchData();
        }}
      />
      <OrderModal
        visible={orderVisible}
        data={curLab}
        onCancel={() => setOrderVisible(false)}
        onSuccess={() => {
          setOrderVisible(false);
        }}
      />
    </div>
  );
}

export default Labs;
