import React, { useCallback, useState, useEffect } from 'react';
import { Modal, Form, Input, TimePicker } from '@arco-design/web-react';
import * as apis from '../../api';

interface LabEditModalProps {
  visible: boolean;
  data?: any;
  onCancel?: () => void;
  onSuccess?: () => void;
}
const OrderModal = ({ visible, data, onCancel, onSuccess }: LabEditModalProps) => {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const username = localStorage.getItem('username');

  const onSubmit = useCallback(() => {
    form.validate(async (errors, values) => {
      if (!errors) {
        try {
          setConfirmLoading(true);
          await apis.addOrder({ ...values, time: values.time.join('-') } as {
            labName: string;
            time: string;
            username: string;
          });
          onSuccess && onSuccess();
        } finally {
          setConfirmLoading(false);
        }
      }
    });
  }, [form]);

  useEffect(() => {
    form.setFieldsValue({ labName: data?.labName });
  }, [form, data]);

  useEffect(() => {
    form.setFieldsValue({ username });
  }, [form, username]);

  return (
    <Modal
      visible={visible}
      title="预约实验室"
      confirmLoading={confirmLoading}
      onCancel={onCancel}
      onOk={onSubmit}
    >
      <Form form={form} labelCol={{ span: 7 }} wrapperCol={{ span: 17 }}>
        <Form.Item
          label="实验室名称"
          field="labName"
          disabled
          required
          rules={[{ required: true, message: '请输入实验室名称' }]}
        >
          <Input placeholder="请输入实验室名称" />
        </Form.Item>
        <Form.Item
          label="预约用户"
          field="username"
          disabled
          required
          rules={[{ required: true, message: '请输入实验室地点' }]}
        >
          <Input placeholder="请输入预约用户" />
        </Form.Item>
        <Form.Item
          label="预约时间"
          field="time"
          required
          rules={[{ required: true, message: '请选择预约时间' }]}
        >
          <TimePicker.RangePicker />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default OrderModal;
