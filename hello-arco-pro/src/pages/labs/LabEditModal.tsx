import React, { useMemo, useCallback, useState, useEffect } from 'react';
import { Modal, Form, Input, InputNumber } from '@arco-design/web-react';
import * as apis from '../../api';

interface LabEditModalProps {
  mode: 'create' | 'edit';
  visible: boolean;
  data?: any;
  onCancel?: () => void;
  onSuccess?: () => void;
}
const LabEditModal = ({ mode, visible, data, onCancel, onSuccess }: LabEditModalProps) => {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const title = useMemo(() => {
    return mode === 'create' ? '新增实验室' : '编辑实验室';
  }, [mode]);

  const onSubmit = useCallback(() => {
    form.validate(async (errors, values) => {
      if (!errors) {
        try {
          setConfirmLoading(true);
          if (mode === 'create') {
            await apis.addLab(values as { labName: string; position: string; deviceNum: number });
          } else {
            await apis.editLab(
              data.id,
              values as { labName: string; position: string; deviceNum: number }
            );
          }
          onSuccess && onSuccess();
        } finally {
          setConfirmLoading(false);
        }
      }
    });
  }, [form, mode]);

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(data);
  }, [data, visible]);

  return (
    <Modal
      visible={visible}
      title={title}
      confirmLoading={confirmLoading}
      onCancel={onCancel}
      onOk={onSubmit}
    >
      <Form form={form} labelCol={{ span: 7 }} wrapperCol={{ span: 17 }}>
        <Form.Item
          label="实验室名称"
          field="labName"
          required
          rules={[{ required: true, message: '请输入实验室名称' }]}
        >
          <Input placeholder="请输入实验室名称" />
        </Form.Item>
        <Form.Item
          label="实验室地点"
          field="position"
          required
          rules={[{ required: true, message: '请输入实验室地点' }]}
        >
          <Input placeholder="请输入实验室地点" />
        </Form.Item>
        <Form.Item
          label="可用设备数量"
          field="deviceNum"
          required
          rules={[{ required: true, message: '请输入可用设备数量' }]}
        >
          <InputNumber placeholder="请输入可用设备数量" min={0} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default LabEditModal;
