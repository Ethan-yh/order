import React, { useEffect, useState, useCallback } from 'react';
import { Table, Button, Breadcrumb, Card } from '@arco-design/web-react';
import styles from './style/index.module.less';
import * as apis from '../../api';
import OrderApproveModal from './OrderApproveModal';

function Orders() {
  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState(false);
  const username = localStorage.getItem('username');
  const [curOrder, setCurOrder] = useState();
  const [approveVisible, setApproveVisible] = useState(false);

  const fetchData = useCallback(async () => {
    try {
      setLoading(true);
      const { data } = await apis.getOrders();
      setData(data);
    } finally {
      setLoading(false);
    }
  }, []);
  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const onApprove = useCallback((data: any) => {
    setCurOrder(data);
    setApproveVisible(true);
  }, []);

  const onDelete = useCallback(
    async (data: any) => {
      await apis.deleteOrder(data.id);
      fetchData();
    },
    [fetchData]
  );

  const columns = [
    {
      title: '序号',
      dataIndex: 'id',
    },
    {
      title: '实验室名称',
      dataIndex: 'labName',
    },
    {
      title: '预约时间',
      dataIndex: 'time',
    },
    {
      title: '用户名',
      dataIndex: 'username',
    },
    {
      title: '审批状态',
      dataIndex: 'status',
    },
    {
      title: '操作',
      dataIndex: 'operations',
      render: (_, record: any) => (
        <div className={styles.operations}>
          <Button
            type="text"
            size="small"
            onClick={() => onApprove(record)}
            disabled={username !== 'admin' || ['pass', 'failed'].includes(record.status)}
          >
            审批
          </Button>
          <Button
            type="text"
            status="danger"
            size="small"
            onClick={() => onDelete(record)}
            disabled={!['admin', record.username].includes(username)}
          >
            删除
          </Button>
        </div>
      ),
    },
  ];

  return (
    <div className={styles.container}>
      <Breadcrumb style={{ marginBottom: 20 }}>
        <Breadcrumb.Item>实验室预约</Breadcrumb.Item>
        <Breadcrumb.Item>预约列表</Breadcrumb.Item>
      </Breadcrumb>
      <Card bordered={false}>
        {/* <div className={styles.toolbar}>
          <div>
            <Button type="primary" onClick={onCreate}>
              添加实验室
            </Button>
          </div>
        </div> */}
        <Table rowKey="id" loading={loading} columns={columns} data={data} />
      </Card>
      <OrderApproveModal
        visible={approveVisible}
        data={curOrder}
        onCancel={() => setApproveVisible(false)}
        onSuccess={() => {
          setApproveVisible(false);
          fetchData();
        }}
      />
    </div>
  );
}

export default Orders;
