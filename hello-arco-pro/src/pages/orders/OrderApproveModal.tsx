import React, { useCallback, useState, useEffect } from 'react';
import { Modal, Form, Input, Select } from '@arco-design/web-react';
import * as apis from '../../api';

interface OrderApproveModalProps {
  visible: boolean;
  data: any;
  onCancel?: () => void;
  onSuccess?: () => void;
}
const OrderApproveModal = ({ visible, data, onCancel, onSuccess }: OrderApproveModalProps) => {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);

  const onSubmit = useCallback(() => {
    form.validate(async (errors, values) => {
      if (!errors) {
        try {
          setConfirmLoading(true);
          await apis.approveOrders(data.id, { status: values.approve });
          onSuccess && onSuccess();
        } finally {
          setConfirmLoading(false);
        }
      }
    });
  }, [form, data]);

  useEffect(() => {
    form.setFieldsValue(data);
  }, [form, data]);

  return (
    <Modal
      visible={visible}
      title="审批预约"
      confirmLoading={confirmLoading}
      onCancel={onCancel}
      onOk={onSubmit}
    >
      <Form form={form} labelCol={{ span: 7 }} wrapperCol={{ span: 17 }}>
        <Form.Item
          label="预约编号"
          field="id"
          required
          disabled
          rules={[{ required: true, message: '请输入预约编号' }]}
        >
          <Input placeholder="请输入预约编号" />
        </Form.Item>
        <Form.Item
          label="实验室名称"
          field="labName"
          disabled
          required
          rules={[{ required: true, message: '请输入实验室名称' }]}
        >
          <Input placeholder="请输入实验室名称" />
        </Form.Item>
        <Form.Item
          label="预约时间"
          field="time"
          disabled
          required
          rules={[{ required: true, message: '请输入预约时间' }]}
        >
          <Input placeholder="请输入预约时间" />
        </Form.Item>
        <Form.Item
          label="预约用户"
          field="username"
          disabled
          required
          rules={[{ required: true, message: '请输入预约用户' }]}
        >
          <Input placeholder="请输入预约用户" min={0} />
        </Form.Item>
        <Form.Item
          label="审批"
          field="approve"
          required
          rules={[{ required: true, message: '请选择审批状态' }]}
        >
          <Select
            placeholder="请选择审批状态"
            options={[
              { label: '通过', value: 'pass' },
              { label: '不通过', value: 'failed' },
            ]}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default OrderApproveModal;
