import { Notification } from '@arco-design/web-react';
import axios from 'axios';
import { apiHost } from './api_url';

const request = async (
  url: string,
  config: { method: 'get' | 'post' | 'put' | 'delete'; data: any; title: string },
  showSuccess = false,
  showError = true
) => {
  const res = await axios({ url, method: config.method, data: config.data });
  if (!(res.data.error_code >= 0)) {
    showError && Notification.error({ title: `${config.title} 失败`, content: res.data.error_msg });
    throw new Error('request error');
  }
  showSuccess && Notification.success({ title: `${config.title} 成功`, content: '' });
  return res.data;
};

export const login = async (data: { username: string; password: string }) => {
  return request(`${apiHost}/login`, { method: 'post', data, title: '登录' }, true);
};

export const register = async (data: { username: string; password: string }) => {
  return request(`${apiHost}/register`, { method: 'post', data, title: '注册' }, true);
};

// 增加实验室
export const addLab = async (data: { labName: string; position: string; deviceNum: number }) => {
  return request(`${apiHost}/labs`, { method: 'post', data, title: '增加实验室' }, true);
};

// 删除实验室
export const deleteLab = async (id: string) => {
  return request(
    `${apiHost}/labs/${id}`,
    { method: 'delete', data: null, title: '删除实验室' },
    true
  );
};

// 编辑实验室
export const editLab = async (
  id: string,
  data: { labName: string; position: string; deviceNum: number }
) => {
  return request(`${apiHost}/labs/${id}`, { method: 'put', data, title: '编辑实验室' }, true);
};

// 查询实验室
export const getLabs = async () => {
  return request(`${apiHost}/labs`, { method: 'get', data: null, title: '查询实验室' });
};

// 增加预约记录
export const addOrder = async (data: { labName: string; time: string; username: string }) => {
  return request(`${apiHost}/orders`, { method: 'post', data, title: '预约实验室' }, true);
};

// 取消预约
export const deleteOrder = async (id: string) => {
  return request(
    `${apiHost}/orders/${id}`,
    { method: 'delete', data: null, title: '删除预约' },
    true
  );
};

// 查询预约
export const getOrders = async () => {
  return request(`${apiHost}/orders`, { method: 'get', data: null, title: '查询预约记录' });
};

// 审核预约
export const approveOrders = async (id: string, data: { status: string }) => {
  return request(
    `${apiHost}/orders/${id}/approve`,
    { method: 'post', data, title: '审批预约' },
    true
  );
};
